FROM nginx
RUN apt-get update
RUN apt-get install -y net-tools
WORKDIR /app
COPY ./Website/index.html  /usr/share/nginx/html/
COPY . /app
EXPOSE 80
